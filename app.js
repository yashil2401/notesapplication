showNotes();
let addNoteBtn = document.getElementById("addNoteBtn");
addNoteBtn.addEventListener("click", function (e) {
    let textArea = document.getElementById("textArea");
    let notes = localStorage.getItem("notes");
    if (notes == null) {
        notesVar = [];
    }
    else {
        notesVar = JSON.parse(notes);
    }
    notesVar.push(textArea.value);
    localStorage.setItem("notes", JSON.stringify(notesVar));
    textArea.value = " ";
    showNotes();
})
function showNotes() {
    let notes = localStorage.getItem("notes");
    if (notes == null) {
        notesVar = [];
    }
    else {
        notesVar = JSON.parse(notes);
    }
    let html = "";
    notesVar.forEach(function (element, index) {
        html += `
        <div class="noteCard my-2 mx-2 card" style="width: 18rem;">
        <div class="card-body">
            <h5 class="card-title">Note ${index + 1}</h5>
            <p class="card-text">${element}</p>
            <button id="${index}"onClick="deleteNote(this.id)" class="btn btn-primary btn-sm">Delete</button>
        </div>
    </div>
        `;
    })
    let notesElmnt = document.getElementById("notes");
    if (notesVar.length != 0) {
        notesElmnt.innerHTML = html;
    }
    else {
        notesElmnt.innerHTML = `Nothing to show! Use "Add Note" to add notes here`;
    }
}
function deleteNote(index) {
    let notes = localStorage.getItem("notes");
    if (notes == null) {
        notesVar = [];
    }
    else {
        notesVar = JSON.parse(notes);
    }
    notesVar.splice(index, 1);
    localStorage.setItem("notes", JSON.stringify(notesVar));
    showNotes();
}
let search = document.getElementById("searchText");
search.addEventListener("input", function () {
    let inputVal = search.value.toLowerCase();
    let noteCards = document.getElementsByClassName("noteCard");
    Array.from(noteCards).forEach(function (element) {
        let cardTxt = element.getElementsByTagName("p")[0].innerText;
        if (cardTxt.includes(inputVal)) {
            element.style.display = "block";
        }
        else {
            element.style.display = "none";
        }
    })
})
